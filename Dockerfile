FROM ubuntu:trusty

RUN apt-get update && apt-get -y install libssl-dev build-essential g++-4.4 libboost-all-dev libsparsehash-dev git-core perl vim byobu
COPY . blockparser
RUN cd blockparser && ./make
ENTRYPOINT ["blockparser/parser"]
CMD ["all", "-c", "-w", "100000000"]
